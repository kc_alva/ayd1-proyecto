import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Comercio2Page } from '../comercio2/comercio2'
import { MapaPage } from '../mapa/mapa'
import { Geolocation } from 'ionic-native';

/*
  Generated class for the Comercio1 page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-comercio1',
  templateUrl: 'comercio1.html'
})
export class Comercio1Page {
  public direccion;
  public nombre;
  public web;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  sig(){
    this.navCtrl.push(Comercio2Page,{
      nombre: this.nombre, web:this.web, direccion: this.direccion
    });
  }

}
