import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Restaurants } from '../../providers/restaurants';
import {Userdata} from '../../providers/userdata'
import { CreateActivityPage } from '../create-activity/create-activity';
import { RestaurantProductsPage } from '../restaurant-products/restaurant-products';
/*
  Generated class for the Restaurantes page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-restaurantes',
  templateUrl: 'restaurantes.html',
   providers:[Restaurants, Userdata]
})
export class RestaurantesPage {

  public listRest = [];
  public items=[];
  contitem=0;
  logout=false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public restaurant: Restaurants, public userData:Userdata) {
    this.loadRestaurants();
  }

  loadRestaurants(){
    return new Promise(resolve=>{
      this.restaurant.getRestaurants().on('value', snapshot=>{
        let list = [];
        snapshot.forEach(snap=>{
          list.push({
            key: snap.key,
            nombre: snap.val().nombre,
            direccion:snap.val().direccion,
            web: snap.val().web,
            imagen:snap.val().imagen
          });
        });
        this.listRest = list.reverse();
        for(let i=0; this.contitem<this.listRest.length;i++){
          this.items.push(this.listRest[this.contitem]);
          this.contitem++;
        }
        resolve(true);
      });
    });
  }

  checkRestaurant():boolean{
    if(!this.logout){
      if(this.userData.checkRestaurant()){
        return true;
      }
      else{
        return false;
      }
    }
    else{
      return false;
    }
  }

  createActivity(){
    this.navCtrl.push(CreateActivityPage);
  }

  restaurantsProducts(key){
    this.navCtrl.push(RestaurantProductsPage, {
      key: key
    })
  }

}
