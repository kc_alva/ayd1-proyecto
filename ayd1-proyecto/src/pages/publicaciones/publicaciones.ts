import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Restaurants } from '../../providers/restaurants';

import firebase from 'firebase';

import { ProductDetailPage } from '../product-detail/product-detail';
/*
  Generated class for the Publicaciones page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-publicaciones',
  templateUrl: 'publicaciones.html',
  providers:[Restaurants]
})
export class PublicacionesPage {

  producto;
  products = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public restaurant: Restaurants) {
    this.loadProducts();
  }

  loadProducts(){
      this.producto=firebase.database().ref('/productos/');
      this.producto.on('value', snapshot=>{
        let list = [];
        snapshot.forEach(snap=>{
          list.push({
            key: snap.key,
            name: snap.val().name,
            price:snap.val().price,
            description: snap.val().description,
            image:snap.val().image
          });
        });
        this.products=list;
    });
  }

  viewDetail(id){
    this.navCtrl.push(ProductDetailPage, {
      id: id
    })
  }
}
