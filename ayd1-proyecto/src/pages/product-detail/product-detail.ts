import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Restaurants } from '../../providers/restaurants';
import { AngularFire, FirebaseObjectObservable,AuthProviders,AuthMethods} from 'angularfire2';

import firebase from 'firebase';

/*
  Generated class for the ProductDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-product-detail',
  templateUrl: 'product-detail.html',
  providers:[Restaurants]
})
export class ProductDetailPage {
  productKey;
  name;
  price;
  description;
  image;
  key;
  comentario;
  currentUser;
  comment;
  comments=[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public restaurant: Restaurants) {
    this.currentUser=firebase.auth().currentUser.uid;
    this.productKey=this.navParams.get('id');
    this.displayProduct(this.productKey);
    this.getComments(this.productKey);
  }

  displayProduct(id){
    this.restaurant.viewProduct(id).then(snapshot=>{
      this.name=snapshot.val().name;
      this.price=snapshot.val().price;
      this.description=snapshot.val().description;
      this.image=snapshot.val().image;
      this.key=snapshot.key;
    })
    
  }

  addComment(key){
    if(this.comentario!=null){
      var comment = {
        text: this.comentario,
        user: this.currentUser
      }

      this.restaurant.addComment(key, comment);
      this.comentario='';
    }
  }

  getComments(key){
    console.log("hola",key);
    this.comment=firebase.database().ref('/productos/'+key+'/comments/');
      this.comment.on('value', snapshot=>{
        let list = [];
        snapshot.forEach(snap=>{
          list.push({
            key: snap.key,
            text: snap.val().text,
            username:snap.val().username,
            userImage: snap.val().userImage,
          });
        });
        this.comments=list;
    });
  }

}
