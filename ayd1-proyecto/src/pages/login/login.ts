import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { PrincipalPage } from '../principal/principal';
import { RegistroPage } from '../registro/registro';
import { Userdata } from '../../providers/userdata';
import {AngularFire} from 'angularfire2';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers:[Userdata]
})


// Clase generada por Ionic para el LoginPage.
export class LoginPage {
  email:string;
  password:string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public af: AngularFire, public userData:Userdata, private toastCtrl:ToastController) {
  }


// Obteniendo email y login desde la base de datos.
  login(){
    var credentials = {email:this.email, password:this.password};
    this.userData.loginUser(credentials).then((data)=>{
      this.presentToast('Bienvenido');
    }).catch((error)=>{
      console.log(error);
	// Enviar error si los datos no son correctos.
      this.presentToast('ERROR: Verifica tu email o contraseña');
    });
  }


// Aqui se llama a la pagina de registro dentro del layout.
  registro(){
    this.navCtrl.setRoot(RegistroPage);
  }


// Configuracion del toast para esta pagina. 
  presentToast(message:string) {
  let toast = this.toastCtrl.create({
    message: message,
    duration: 3000,
    position: 'top'
  });
  toast.onDidDismiss(() => {
  });
  toast.present();
  }
}
