import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Restaurants } from '../../providers/restaurants';

import firebase from 'firebase';

import { ProductDetailPage } from '../product-detail/product-detail';

/*
  Generated class for the RestaurantProducts page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-restaurant-products',
  templateUrl: 'restaurant-products.html',
  providers:[Restaurants]
})
export class RestaurantProductsPage {

  producto;
  products = [];
  restaurante=[];
  rest;

  constructor(public navCtrl: NavController, public navParams: NavParams,  public restaurant: Restaurants) {
    this.rest = this.navParams.get('key');
    console.log(this.restaurante);
    this.loadProducts();
  }

  loadProducts(){
      this.producto=firebase.database().ref('/productos/');
      this.producto.on('value', snapshot=>{
        let list = [];
        snapshot.forEach(snap=>{
          list.push({
            key: snap.key,
            name: snap.val().name,
            price:snap.val().price,
            description: snap.val().description,
            image:snap.val().image,
            owner: snap.val().owner
          });
        });
        this.products=list;
    });
  }


  viewDetail(id){
    this.navCtrl.push(ProductDetailPage, {
      id: id
    })
  }

  validate(product):boolean{
    console.log(this.rest, product);
    firebase.database().ref('/restaurantes/'+this.rest+'/products').on('value', snapshot=>{
      let list = [];
      snapshot.forEach(snap=>{
        list.push({
          id:snap.val().id,
          key: snap.key
        });
        return false;
      })
      this.restaurante=list;
    })

    let p = false;
    for(let i=0; i<this.restaurante.length; i++){
      if(product = this.restaurante[i].id){
        p = true;
      }
    }
    
    return p;
  }

}
