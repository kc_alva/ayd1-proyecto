import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Userdata} from '../../providers/userdata'
import firebase from 'firebase';
import { AngularFire, FirebaseObjectObservable,AuthProviders,
  AuthMethods} from 'angularfire2';

  import { RestaurantesPage } from '../restaurantes/restaurantes';
  import { PublicacionesPage } from '../publicaciones/publicaciones';

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
  providers:[Userdata]
})


// Los estandares de codigo no se pueden aplicar a clases que genera Ionic.
export class DashboardPage {
  tab1Root:any;
  tab2Root:any;
  public username: string;
  public nombreUsuario: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userData:Userdata) {
    this.tab1Root = RestaurantesPage;
    this.tab2Root = PublicacionesPage;
    var userid = firebase.auth().currentUser.uid;
  }
}
