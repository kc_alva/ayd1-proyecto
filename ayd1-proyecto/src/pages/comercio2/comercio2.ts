import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, App, ActionSheetController, LoadingController } from 'ionic-angular';
import { Restaurants } from '../../providers/restaurants';
import { PrincipalPage } from '../principal/principal';
import { Camera,CameraOptions } from 'ionic-native';
import {Userdata} from '../../providers/userdata'
import { DataService } from '../../providers/data.service';
import { AngularFire, FirebaseObjectObservable,AuthProviders, AuthMethods} from 'angularfire2';
import firebase from 'firebase';
/*
  Generated class for the Comercio2 page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-comercio2',
  templateUrl: 'comercio2.html',
  providers:[Restaurants, Userdata, DataService]
})
export class Comercio2Page {
  public nombre;
  public web;
  public direccion;
  public restaurantImage;
  public myuserId;

  constructor(public navCtrl: NavController, public navParams: NavParams, public restaurant: Restaurants, private toastCtrl: ToastController, public _app:App, public actionSheetCtrl: ActionSheetController, public userdata: Userdata, public loadingCtrl: LoadingController, public dataService: DataService) {
    this.nombre=navParams.get("nombre");
    this.web=navParams.get("web");
    this.direccion=navParams.get("direccion");
    console.log(this.nombre, this.direccion, this.web)
    this.myuserId = firebase.auth().currentUser.uid;
  }

  editImage(){
    var self = this;
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Seleccionar Imagen',
      buttons: [
        {
          text: 'Seleccionar imagen de Galeria',
          icon: 'image',
          handler: ()=>{
            self.openCamera(Camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Utilizar camara',
          icon: 'camera',
          handler: ()=>{
            self.openCamera(Camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  openCamera(pictureSourceType: any) {
    var self = this;

    let options: CameraOptions = {
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: pictureSourceType,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 400,
      targetHeight: 400,
      saveToPhotoAlbum: true,
      correctOrientation: true
    };

    Camera.getPicture(options).then(imageData => {
      const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          const slice = byteCharacters.slice(offset, offset + sliceSize);

          const byteNumbers = new Array(slice.length);
          for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }

          const byteArray = new Uint8Array(byteNumbers);

          byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
      };

      let capturedImage: Blob = b64toBlob(imageData, 'image/png');
      self.startUploading(capturedImage);
    }, error => {
      console.log('ERROR -> ' + JSON.stringify(error));
    });
  }

  startUploading(file) {

    let self = this;
    let uid = this.userdata.getUserUid();
    let progress: number = 0;
    // display loader
    let loader = this.loadingCtrl.create({
      content: 'cargando..',
    });
    loader.present();

    // Upload file and metadata to the object 'images/mountains.jpg'
    var fileName = 'restaurantImage-' + new Date().getTime() + '.jpg';
    var metadata = {
      contentType: 'image/png',
      name: fileName,
      cacheControl: 'no-cache',
    };

    var uploadTask = self.dataService.getStorageRef().child('images/users/' + this.myuserId + '/restaurantImage/'+fileName).put(file, metadata);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on('state_changed',
      function (snapshot) {
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      }, function (error) {
        loader.dismiss().then(() => {
          switch (error.code) {
            case 'storage/unauthorized':
              // User doesn't have permission to access the object
              break;

            case 'storage/canceled':
              // User canceled the upload
              break;

            case 'storage/unknown':
              // Unknown error occurred, inspect error.serverResponse
              break;
          }
        });
      }, function () {
        loader.dismiss().then(() => {
          // Upload completed successfully, now we can get the download URL
          var downloadURL = uploadTask.snapshot.downloadURL;
          self.userdata.setRestImage(downloadURL);
          self.presentToast(downloadURL);
          self.reload();
            //self.dataService.setUserImage(uid);
        });
      });
  }

  reload(){
    this.userdata.getUserData().subscribe(snap=>{
      this.restaurantImage=snap.restaurantImage;   
    });
  }

  guardar(){
    let rest={
      nombre: this.nombre,
      web:this.web,
      direccion:this.direccion,
      imagen: this.restaurantImage
    }
    this.restaurant.uploadRest(rest);
    this.presentToast("Has registrado tu negocio con exito!");
    this._app.getRootNav().setRoot(PrincipalPage);
  }

  presentToast(message:string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
    });

    toast.present();
  }

}
