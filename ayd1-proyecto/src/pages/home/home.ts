import { Component } from '@angular/core';
import { NativeStorage } from 'ionic-native';
import { NavController, Platform } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { RegistroPage} from '../registro/registro'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public platform: Platform) {
    
  }

  login(){
    this.navCtrl.push(LoginPage);
  }

  registro(){
    this.navCtrl.push(RegistroPage);
  }

}
