import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFire, FirebaseObjectObservable,AuthProviders,AuthMethods} from 'angularfire2';
import firebase from 'firebase';

/*
  Generated class for the Restaurants provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Restaurants {
  restaurants;
  public currentUser: any;
  public fireAuth: any;
  user;
  userauth;
  products;

  constructor(public af: AngularFire) {
    this.fireAuth=firebase.auth();
    this.restaurants = firebase.database().ref('/restaurantes/');
    this.products = firebase.database().ref('/productos/');
  }

  uploadRest(restaurante){
    this.currentUser = firebase.auth().currentUser.uid;  
    let refuser=firebase.database().ref('/users/'+this.currentUser);
    let refbb=firebase.database().ref('/restaurantes/');
    refbb.push(restaurante).then(snap=>{
      refuser.child('restaurantes').set(snap.key);
    });
  }

  getRestaurants():any{
    return this.restaurants;
  }

  getProducts():any{
    return this.products;
  }

  createProduct():any{
  return this.products.push().key;  
  }

  addProduct(product, key, restaurant){
    return new Promise((resolve: ()=> void, reject: (reason:Error)=> void)=>{
      var update={};
      update['/productos/'+key]=product;
      firebase.database().ref().update(update).then(newProduct=>{
        firebase.database().ref('/restaurantes/'+restaurant+'/products').push({'id':key});
        resolve();  
      }).catch((error)=>{
        console.log(error);
      });
    });
  }

  viewProduct(id){
    var userRef = this.products.child(id);
    return userRef.once('value');
  }

  addComment(key, comentario){
    this.currentUser = firebase.auth().currentUser.uid;  
    return new Promise((resolve: () => void, reject: (reason: Error) => void) => {
          var refactivity=firebase.database().ref('productos/'+key+'/comments');
          refactivity.push(comentario).then(newcomment=>{
          firebase.database().ref('users/' + this.currentUser).on('value',snap=>{
            this.products.child(key).child('comments').child(newcomment.key).child('username').set(snap.val().username);
            this.products.child(key).child('comments').child(newcomment.key).child('userImage').set(snap.val().userImage);
         });
        resolve();
      }).catch((error) => {
      });
    });
  }

  getComments(){
    
  }
}
