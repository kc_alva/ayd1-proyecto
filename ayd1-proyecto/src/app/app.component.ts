import { Component, NgZone } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import firebase from 'firebase';
import {
  FIREBASE_PROVIDERS, defaultFirebase,
  AngularFire, firebaseAuthConfig
} from 'angularfire2';

import { PrincipalPage } from '../pages/principal/principal';
import { HomePage } from '../pages/home/home';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  zone: NgZone;

  constructor(platform: Platform, af:AngularFire) {

    this.zone = new NgZone({});

    firebase.auth().onAuthStateChanged((user)=>{
      this.zone.run(()=>{
        if(!user){
          this.rootPage=HomePage;
        }else{
          this.rootPage=PrincipalPage;
        }
      });
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }
}
