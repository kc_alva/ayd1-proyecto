import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PrincipalPage } from '../pages/principal/principal';
import { LoginPage } from '../pages/login/login';
import { RegistroPage } from '../pages/registro/registro';
import { AngularFireModule, AuthMethods, AuthProviders } from 'angularfire2';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { RestaurantesPage } from '../pages/restaurantes/restaurantes';
import { PublicacionesPage } from '../pages/publicaciones/publicaciones';
import { FormularioPage } from '../pages/formulario/formulario';
import { Comercio1Page } from '../pages/comercio1/comercio1';
import { Comercio2Page } from '../pages/comercio2/comercio2';
import { MapPage } from '../pages/map/map';
import { CreateActivityPage } from '../pages/create-activity/create-activity';
import { ProductDetailPage } from '../pages/product-detail/product-detail';
import { RestaurantProductsPage } from '../pages/restaurant-products/restaurant-products';
export const firebaseConfig={
  apiKey: "AIzaSyBvqpk6AfVZ0PacyFFufTJ0R3pyO7ETMgo",
  authDomain: "lunchbook-df7e0.firebaseapp.com",
  databaseURL: "https://lunchbook-df7e0.firebaseio.com",
  storageBucket: "lunchbook-df7e0.appspot.com",
  messagingSenderId: "72368821993"
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PrincipalPage,
    LoginPage,
    RegistroPage,
    DashboardPage,
    RestaurantesPage,
    FormularioPage,
    PublicacionesPage,
    Comercio1Page,
    Comercio2Page,
    CreateActivityPage,
    ProductDetailPage,
    RestaurantProductsPage,
    MapPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PrincipalPage,
    LoginPage,
    RegistroPage,
    DashboardPage,
    RestaurantesPage,
    FormularioPage,
    PublicacionesPage,
    Comercio1Page,
    Comercio2Page,
    CreateActivityPage,
    ProductDetailPage,
    RestaurantProductsPage,
    MapPage
  ],
  providers: []
})
export class AppModule {}
